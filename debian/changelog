lxd (5.0.2+git20231211.1364ae4-8) unstable; urgency=medium

  * Update Standards-Version to 4.7.1 in d/control (no changes needed)
  * Update years in d/copyright
  * Add patch to skip a couple of flaky tests (Closes: #1089935)
  * Cherry-pick upstream update of test cert to prevent future FTBFS errors
    during trixie support period (Closes: #1078628)
  * Add patches fixing creation of VMs with newer versions of QEMU (ported
    from Incus)

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 23 Feb 2025 21:30:48 +0000

lxd (5.0.2+git20231211.1364ae4-7) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 17 Aug 2024 07:22:57 -0400

lxd (5.0.2+git20231211.1364ae4-6) experimental; urgency=medium

  * Team upload.
  * Build against go-criu v7, Closes: #1078219

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 08 Aug 2024 22:30:24 -0400

lxd (5.0.2+git20231211.1364ae4-5) unstable; urgency=medium

  * Update the images: remote URL to point to Canonical's server
    (Closes: #1071047)
  * Move a couple of installed files from /lib/ to /usr/lib/ to support the
    /usr merge effort

 -- Mathias Gibbens <gibmat@debian.org>  Wed, 12 Jun 2024 10:41:51 +0000

lxd (5.0.2+git20231211.1364ae4-4) unstable; urgency=medium

  * Add patch to fix building with zitadel/oidc/v3
  * Remove dropped lintian override for lxd-agent
  * Update d/watch to reflect new LTS versioning scheme

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 11 Apr 2024 00:49:32 +0000

lxd (5.0.2+git20231211.1364ae4-3) unstable; urgency=medium

  * Add a NEWS entry mentioning LXD's relicensing and recommending users
    switch to Incus
  * Add a patch to fix apparmor profile generation for QEMU instances (copied
    from Incus)
  * d/control:
    - Add Conflicts: incus-tools for lxd-tools
  * Rename d/README.debian -> d/README.Debian

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 21 Jan 2024 21:25:04 +0000

lxd (5.0.2+git20231211.1364ae4-2) unstable; urgency=medium

  * Fix autopkgtest
  * Add a patch to fix QEMU detection
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Mon, 08 Jan 2024 23:50:45 +0000

lxd (5.0.2+git20231211.1364ae4-1) unstable; urgency=medium

  * Import snapshot of upstream stable-5.0 with final commit prior to upstream
    changing licensing to AGPL-3.0 (see #1058592 for more information)
    - Drop patches previously applied upstream
    - Refresh remaining patches as needed
  * d/control:
    - Update Build-Depends
    - Update XS-Go-Import-Path to reflect upstream project movement
    - Add a golang-github-canonical-lxd-dev binary package
  * Update d/rules reference of project's gopath
  * Add a note to d/README.source about LXD's licensing changes

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 21 Dec 2023 01:23:27 +0000

lxd (5.0.2-6) unstable; urgency=medium

  * Update URLs to reflect movement of LXD project
  * Cherry-pick upstream patch for building on loong64

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 23 Nov 2023 14:37:00 +0000

lxd (5.0.2-5) unstable; urgency=medium

  * Add missing Replaces in d/control for lxd-client to fix potential bullseye
    upgrade issue (Closes: #1034971)

 -- Mathias Gibbens <gibmat@debian.org>  Fri, 05 May 2023 12:42:21 +0000

lxd (5.0.2-4) unstable; urgency=medium

  * Cherry-pick upstream fix for qemu >= 7.2 (Closes: #1030365)

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 23 Apr 2023 17:50:08 +0000

lxd (5.0.2-3) unstable; urgency=medium

  * Cherry-pick upstream commit to fix issue with btrfs-progs >= 6.0
    (Closes: #1032482)
  * Document known issue with virtual machines not working with QEMU 7.2
    (See: #1030365)

 -- Mathias Gibbens <gibmat@debian.org>  Wed, 08 Mar 2023 02:27:33 +0000

lxd (5.0.2-2) unstable; urgency=medium

  * Fix mipsel and mips64el builds

 -- Mathias Gibbens <gibmat@debian.org>  Wed, 18 Jan 2023 02:17:18 +0000

lxd (5.0.2-1) unstable; urgency=medium

  * New upstream release:
    - Drop patches applied upstream
    - Refresh remaining patches
    - Add patch to temporarily revert use of go-criu v6
  * Update Build-Depends in d/control

 -- Mathias Gibbens <gibmat@debian.org>  Tue, 17 Jan 2023 03:35:04 +0000

lxd (5.0.1-5) unstable; urgency=medium

  * d/control:
    - Build-Depend on golang-github-golang-protobuf-1-5-dev
    - Add Build-Conflicts: golang-github-golang-protobuf-1-3-dev
    - Update minimum version of gobgp
    - Remove tomoyo-tools from list of Suggested packages
  * Refresh d/copyright
  * Update mention of dnsmasq to dnsmasq-base in d/README.debian

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 08 Jan 2023 03:22:20 +0000

lxd (5.0.1-4) unstable; urgency=medium

  * d/control:
    - Bump Standards-Version (no changes needed)
    - Build-Depend on gobgp 3.x; drop patch for gobgp 2.x
    - Recommend dnsmasq-base rather than a full dnsmasq install

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 29 Dec 2022 00:01:16 +0000

lxd (5.0.1-3) unstable; urgency=medium

  [ Mathias Gibbens ]
  * Update my email address
  * Update d/README.debian:
    - Add a note about running LXD and Docker on the same host
    - Update documentation for running LXD on non-systemd hosts
    - Add basic configuration instructions
  * Fix salsa CI autopkgtest job failure
  * Work on making builds reproducible
  * Cherry-pick upstream commit fixing referenced configuration directory
    (Closes: #1021624)
  * Move lxd-agent to Recommends from Suggests
  * Move dnsmasq to Recommends from Depends (Closes: #1025432)

  [ Michael Jeanson ]
  * Clean 'man1/' created in d/rules

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 11 Dec 2022 17:05:51 +0000

lxd (5.0.1-2) unstable; urgency=medium

  * Ignore lintian error about statically-linked lxd-agent binary
  * Enable hardening build flags in d/rules
  * Add patch to fix FTBFS on mips64el and mipsel
  * Add patch to fix FTBFS on armel and armhf

 -- Mathias Gibbens <mathias@calenhad.com>  Wed, 14 Sep 2022 02:15:22 +0000

lxd (5.0.1-1) unstable; urgency=medium

  * New upstream release
    - Add/delete/refresh patches as needed
    - Cherry-pick upstream commits to fix xattr test
  * Update Build-Depends in d/control
  * Exclude devlxd-client from building in d/rules

 -- Mathias Gibbens <mathias@calenhad.com>  Sun, 11 Sep 2022 19:30:25 +0000

lxd (5.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #768073)

 -- Mathias Gibbens <mathias@calenhad.com>  Thu, 04 Aug 2022 17:09:19 +0530
