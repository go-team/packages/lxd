---
relatedlinks: '[How&#32;to&#32;install&#32;a&#32;Windows&#32;11&#32;VM&#32;using&#32;LXD](https://ubuntu.com/tutorials/how-to-install-a-windows-11-vm-using-lxd)'
---

(instances)=
# Instances

```{toctree}
:titlesonly:

explanation/instances.md
Create instances <howto/instances_create.md>
Manage instances <howto/instances_manage.md>
Configure instances <howto/instances_configure.md>
Create snapshots <howto/instances_snapshots.md>
Use profiles <profiles.md>
Use cloud-init <cloud-init>
Run commands <instance-exec.md>
Access the console <howto/instances_console.md>
Access files <howto/instances_access_files.md>
Add a routed NIC to a VM </howto/instances_routed_nic_vm.md>
Troubleshoot errors <howto/instances_troubleshoot.md>
explanation/instance_config.md
```
