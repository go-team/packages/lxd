---
relatedlinks: '[Directly&#32;interacting&#32;with&#32;the&#32;LXD&#32;API](https://ubuntu.com/blog/directly-interacting-with-the-lxd-api)'
---

# REST API

```{toctree}
:maxdepth: 1

Main API documentation <rest-api>
api
Main API extensions <api-extensions>
Instance API documentation <dev-lxd>
Events API documentation <events>
Metrics API documentation <metrics>
```
